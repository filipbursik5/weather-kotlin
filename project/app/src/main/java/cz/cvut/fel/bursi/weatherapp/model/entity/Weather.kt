package cz.cvut.fel.bursi.weatherapp.model.entity

/**
 * Created by Filip on 19.07.2017.
 */

class Weather(
        val main: String,
        val description: String
)