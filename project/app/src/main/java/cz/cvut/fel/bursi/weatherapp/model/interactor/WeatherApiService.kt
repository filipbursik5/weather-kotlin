package cz.cvut.fel.bursi.weatherapp.model.interactor

import cz.cvut.fel.bursi.weatherapp.model.`interface`.IWeatherApi
import cz.cvut.fel.bursi.weatherapp.model.entity.WeatherJSON
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

/**
 * Created by Filip on 19.07.2017.
 */

class WeatherApiService {

    val BASE_URL : String = "https://www.reddit.com"
    val weatherApiService: IWeatherApi

    init {
        val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(MoshiConverterFactory.create())
                .build()

        weatherApiService = retrofit.create(IWeatherApi::class.java)
    }

    fun getWeather(lat: Double, lon: Double): Call<WeatherJSON> {
        return weatherApiService.getWeatherByCoordinates(lat, lon)
    }
}