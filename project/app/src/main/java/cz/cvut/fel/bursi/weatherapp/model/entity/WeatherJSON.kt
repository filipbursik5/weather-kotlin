package cz.cvut.fel.bursi.weatherapp.model.entity

/**
 * Created by Filip on 19.07.2017.
 */

// Entity for GSON

class WeatherJSON(
        val sys: Sys,
        val weather: Weather,
        val main: Main,
        val wind: Wind,
        val clouds: Cloud,
        val name: String
)