package cz.cvut.fel.bursi.weatherapp.ui.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import cz.cvut.fel.bursi.weatherapp.R
import cz.cvut.fel.bursi.weatherapp.model.interactor.WeatherApiService

class MainActivity : AppCompatActivity() {

    val weatherApi: WeatherApiService = WeatherApiService()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
