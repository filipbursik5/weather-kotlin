package cz.cvut.fel.bursi.weatherapp.model.entity

/**
 * Created by Filip on 19.07.2017.
 */

class Main(
        val temp: Double,
        val humidity: Double,
        val pressure: Double,
        val temp_min: Double,
        val temp_max: Double
)