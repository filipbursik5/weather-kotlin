package cz.cvut.fel.bursi.weatherapp.model.`interface`

/**
 * Created by Filip on 19.07.2017.
 */

import cz.cvut.fel.bursi.weatherapp.model.entity.WeatherJSON
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface  IWeatherApi {

    // Define endpoints

    @GET("/weather")
    fun getWeatherByCoordinates(@Query("lat") lat: Double,
               @Query("lon") lon: Double)
            : Call<WeatherJSON>;

}