package cz.cvut.fel.bursi.weatherapp.model.entity

/**
 * Created by Filip on 19.07.2017.
 */

class Wind(
        val sys: Sys,
        val title: String,
        val num_comments: Int,
        val created: Long,
        val thumbnail: String,
        val url: String
)